PLOT REPORT
-----------

Report File    : C:\Users\minao\Documents\CAD_Data\DesignSpark\WakayamaRB\BTShield201405181720 (PCB - PLOT REPORT).txt
Report Written : Monday, May 19, 2014
Design Path    : C:\Users\minao\Documents\CAD_Data\DesignSpark\WakayamaRB\BTShield201405181720.pcb
Design Title   : 
Created        : 2014/04/23 1:29:06
Last Saved     : 2014/05/19 4:27:00
Editing Time   : 600 min
Units          : mm (precision 3)


Device Settings
===============


Gerber Settings
===============

    Leading zero suppression.
    G01 assumed throughout.
    Line termination <*> <CR> <LF>.
    3.4 format absolute mm.
    Format commands defined in Gerber file.
    Aperture table defined in Gerber file.
    Hardware arcs allowed.


Drill Settings
==============

    Excellon Format 1    Format 3.4 absolute in mm.
    No zero suppression.


Plots Output
============


Gerber Output
=============

C:\Users\minao\Documents\CAD_Data\DesignSpark\WakayamaRB\BTShield201405181720 - Top Silkscreen.gbr

    Layers:
    ----------------
    Top Silkscreen

    D-Code  Shape  (mm)
    -------------------
    D21     Round 0.127
    D77     Round 0.025

Gerber Output
=============

C:\Users\minao\Documents\CAD_Data\DesignSpark\WakayamaRB\BTShield201405181720 - Top Copper.gbr

    Layers:
    ------------
    Top Copper

    D-Code  Shape  (mm)
    --------------------------------------------
    D21     Round 0.127
    D22     Round 0.200
    D25     Rectangle 0.800 X 0.800  Rotation 90
    D26     Round 1.524
    D74     Round 1.000
    D75     Round 0.600
    D76     Round 0.300
    D84     Rectangle 0.700 X 1.200
    D85     Rectangle 0.700 X 1.200  Rotation 90
    D86     Rectangle 1.000 X 1.800
    D87     Rectangle 1.000 X 2.400

Gerber Output
=============

C:\Users\minao\Documents\CAD_Data\DesignSpark\WakayamaRB\BTShield201405181720 - Documentation.gbr

    Layers:
    ----------------
    Documentation
    Board Outlines

    D-Code  Shape  (mm)
    -------------------
    D22     Round 0.200

Gerber Output
=============

C:\Users\minao\Documents\CAD_Data\DesignSpark\WakayamaRB\BTShield201405181720 - Top Solder Mask.gbr

    Layers:
    -----------------
    Top Solder Mask

    D-Code  Shape  (mm)
    --------------------------------------------
    D14     Rectangle 0.952 X 0.952  Rotation 90
    D15     Round 1.676
    D16     Rectangle 0.852 X 1.352
    D17     Rectangle 0.852 X 1.352  Rotation 90
    D18     Rectangle 1.152 X 1.952
    D19     Rectangle 1.152 X 2.552

Gerber Output
=============

C:\Users\minao\Documents\CAD_Data\DesignSpark\WakayamaRB\BTShield201405181720 - Bottom Copper.gbr

    Layers:
    ---------------
    Bottom Copper

    D-Code  Shape  (mm)
    --------------------------------------------
    D21     Round 0.127
    D22     Round 0.200
    D23     Rectangle 0.800 X 0.800
    D25     Rectangle 0.800 X 0.800  Rotation 90
    D26     Round 1.524
    D27     Rectangle 0.550 X 1.000  Rotation 90
    D28     Rectangle 3.200 X 3.200  Rotation 90
    D29     Bullet 0.800 X 0.600
    D72     Round 0.800
    D73     Bullet 0.800 X 0.600  Rotation 180
    D74     Round 1.000
    D75     Round 0.600
    D76     Round 0.300

Gerber Output
=============

C:\Users\minao\Documents\CAD_Data\DesignSpark\WakayamaRB\BTShield201405181720 - Bottom Silkscreen.gbr

    Layers:
    -------------------
    Bottom Silkscreen

    D-Code  Shape  (mm)
    -------------------
    D21     Round 0.127
    D77     Round 0.025

Gerber Output
=============

C:\Users\minao\Documents\CAD_Data\DesignSpark\WakayamaRB\BTShield201405181720 - Bottom Solder Mask.gbr

    Layers:
    --------------------
    Bottom Solder Mask

    D-Code  Shape  (mm)
    --------------------------------------------
    D13     Rectangle 0.952 X 0.952
    D14     Rectangle 0.952 X 0.952  Rotation 90
    D15     Round 1.676
    D78     Rectangle 0.702 X 1.152  Rotation 90
    D79     Rectangle 3.352 X 3.352  Rotation 90
    D80     Bullet 0.952 X 0.752
    D81     Round 0.952
    D82     Bullet 0.952 X 0.752  Rotation 180

NC Drill Output
===============

C:\Users\minao\Documents\CAD_Data\DesignSpark\WakayamaRB\BTShield201405181720 - Drill Data - Through Hole.drl

    Output:
    --------------------------
    Plated Round Drill Holes
    Plated Slots

    Tool   Size         Count    ID
    -------------------------------
    T001   000.3000 mm  12     D
    T002   000.6000 mm  9      F
    T003   000.9000 mm  20     I
    -------------------------------
    Total               41
    -------------------------------
    Tool   Size  Count    ID
    ------------------------
    ------------------------
    Total        0
    ------------------------

End Of Report.
